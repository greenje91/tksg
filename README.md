# tksg

A taikyoku shogi game written in Javascript/HTML

## About

Taikyoku shogi is a large shogi variant played on a 36x36 board. Each player starts play with 402 pieces. The object of the game is to capture all of the opposing kings and princes. There are no rules for check or checkmate, but in practice a player will resign when checkmated.

As you might expect with a game containing 804 pieces played on a board with 1,296 squares, a single game of taikyoku shogi can take quite a long time. For that reason, I found the typical chess format of synchronous play to be unsuitable for a game like this. Instead, I designed the game to be played asynchronously, one turn at a time, over the course of weeks, months, or years.

I also felt that to perpetuate a game of this magnitude, playing with someone destined to remain a stranger would be an obstacle. I am therefore very intentional about the need to pass a game file between the two players. It's meant to be something that two people can do to keep in touch or to get to know one another.

## Usage

At the moment, the game is unplayable. The intended usage is to open the HTML file in a browser and begin a new game. After the first player takes their turn, they will be able to export an HTML file containing the new board. They'll send this to their opponent, who will make their move and send a new board back. This repeats until one player wins the game.

## Miles to go

Wow, what isn't left to do? I have a few more pieces that need to have their move logic programmed. After that comes building the export functionality, the new game prompt, and the piece finder. After that, I just need to find someone to play with!