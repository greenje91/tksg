=== to-do ===

refactor or slim down the gameplay code - it's fun and the code badly needs it
    mover is a universal variable now - does that simplify the moveSprite calls?

improve ui
    set tooltips to be on one line(?)
    put unit code labels on pieces
    tune board space colors, or better yet, implement board customization
complete gameplay
    implement alternate promotion logic (promote on first capture)
    after a move is selected, a box appears to finalize the move
    once finalized, the board is locked and the export button shows up
implement paradox options (different movesets and so on)
    i think this is important (aka not optional) as a record of all the different ways the game can be played as a result of imperfect and conflicting information
build info box
    include promotion info, name, movement logic, etc.
build export functionality
    roll everything into one html file
    take finalized board state and export it into a new board
    make sure to switch sides
    make sure game logic is side-independent
write instructions to be included in the html
    maybe have a little introduction, then a button to load the board
    another button to export a new game

optional
    make 'less important' pieces smaller, a la the physical shogi


=== through-talking ===
SW-0243-8520-4052

=== other notes ===
so in terms of tracking pieces: there needs to be an ability to track each piece individually. this will let us pick out each piece as it moves and describe its movement. additionally, we need information that lets us place pieces initially. i suppose the way to do this is to expand the starting position array to include both sets of pieces and just treat it as the determinant array. the positions are set initially but change with the first move, etc.

the way to display information is difficult as well. putting tooltips on the pieces is important, so players don't have to memorize where all their pieces (or their opponents' pieces) are. additionally, having an info box displaying characteristics of selected pieces saves players from having to remember every single moveset of the pieces. the other important piece of information is the piece's promotion potential, so that should go in the infobox as well. building the tooltips with mouseover and mouseleave events should suffice, but the info box is going to have to be its own html fiasco.

another big hurdle is exporting the board as a single html file to be sent to the other player. that's a ways down the road yet, but i want to include a record of the move, like so: [piece moved] [initial position] -> [end position], with a little note if a capture was made.

something i just thought of in relation to how the data is presented...if it's consistent throughout each large array, couldn't it be an array array rather than an object array? do we have to mess with indexes and keys and all that shit? something to think about. another, later note on this thought is that the kanji is not strictly necessary for gameplay, and could reduce the overall size of the file, which is large. that being said, removing the english names would save more space and be less appropriative. on the other hand, the file will get smaller naturally through the course of gameplay, which is cool.