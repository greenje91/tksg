//universal variables
let mode = "sen";
let mover;
let stepMoves = [];
let jumpMoves = [];
let afterjumpMoves = [];
let rangeCapture = [];
let moveComplete = false;

//build board and unit grids
function gameplayBuild() {
    //build board div and grid
    let board = document.createElement('div');
    board.id = 'board';
    document.querySelector('body').appendChild(board);
    gridBuild();
}

//build the grid for the board
function gridBuild() {
    let board = document.querySelector('#board');
    for (y = 0; y < 36; y++) {
        for (x = 0; x < 36; x++) {
            let cell = document.createElement('div');
            cell.classList.add('gridsquare');
            cell.id = `g${x + 1}-${y + 1}`;
            if ((x + y) % 2 == 0) {
                cell.classList.add('coloron');
            } else {cell.classList.add('coloroff');}
            board.appendChild(cell);
        }
    }
    //next step
    unitSetup();
}

//set initial unit positions
//uh i feel like this could be smaller
function unitSetup() {
    //build pieces and arrange them on the grid
    for (j = 0; j < pieces.length; j++) {
        //build piece
        const piece = document.createElement('div');
        piece.id = `${pieces[j].id}`;
        piece.classList.add('piece');
        if (pieces[j].side == "go") {
            piece.classList.add('opponent');
        } else if (pieces[j].side == mode) {
            piece.addEventListener("click", movement);
        }
        //add piece to grid
        let pos = document.querySelector(`#g${pieces[j].coords[1]}-${pieces[j].coords[0]}`);
        pos.appendChild(piece);
        //insert unicode shogi
        const label = document.createElement('p');
        label.innerHTML = "☗";
        label.classList.add('label');
        piece.appendChild(label);
        //add tooltip text
        let unit = pieceData.find(obj => {
            return obj.code == pieces[j].unit;
        })
        const tooltip = document.createElement('p');
        tooltip.id = `tooltip-${pieces[j].id}`;
        tooltip.classList.add('tooltip');
        tooltip.innerHTML = unit.name;
        piece.appendChild(tooltip);
    }
    //next step
    tooltips();
}

//build piece label tooltips
function tooltips() {
    const boardPieces = document.querySelectorAll('.piece');
    for (j = 0; j < boardPieces.length; j++) {
        let tip = document.querySelector(`#tooltip-${boardPieces[j].id}`);
        //fix opponent tooltips
        if (tip.parentElement.classList.contains('opponent')) {tip.style.transform = "scaleY(-1)"}
        boardPieces[j].addEventListener('mouseover', () => {tip.style.display = 'block';}, false);
        boardPieces[j].addEventListener('mouseleave', () => {tip.style.display = 'none'}, false);
    }
    //next step
    checkMode();
}

function checkMode() {
    if (mode == "sen") {
        document.querySelector('#board').style.transform = "scale(1)";
    } else if (mode == "go") {
        document.querySelector('#board').style.transform = "scale(-1)";
    }
}

//build game
gameplayBuild();

//unit movement
function movement() {
    //only one unit moves at a time
    resetMoving();
    //set the active unit
    mover = pieces.find(obj => {
        return obj.id == this.id;
    });
    document.querySelector(`#g${mover.coords[1]}-${mover.coords[0]}`).classList.add('highlight');
    //build array of possible tiles to move to
    getMoveLogic();
    //indicate legal move squares
    let x = mover.coords[1];
    let y = mover.coords[0];
    let newX, newY, target, other;
    let moverPiece = findPiece(mover);
    //for step and ranging pieces
    if (stepMoves != []) {
        //check steps in each direction
        for (s = 0; s < stepMoves.length; s++) {
            let blocked = false;
            if (stepMoves[s] != 0) {
                //check every square for allowed distance
                for (r = 1; r <= stepMoves[s]; r++) {
                    newX = pos(s, r, x, y)[0];
                    newY = pos(s, r, x, y)[1];
                    target = document.querySelector(`#g${newX}-${newY}`);
                    if (target && !blocked) {
                        //check to see if there's a piece, and whether it's friendly or not
                        //this is insane, make this not shitty
                        if (target.children.length != 0) {
                            other = findPiece(target.children[0]);
                            if (moverPiece.side != other.side) {
                                addMoveSquare(newX, newY);
                            }
                            blocked = true;
                        } else {
                            addMoveSquare(newX, newY);
                        }
                    } else {
                        blocked = true;
                    }
                }
            }
        }
    }
    //for jumping pieces
    if (jumpMoves != []) {
        for (j = 0; j < jumpMoves.length; j++) {
            if (jumpMoves[j] != 0) {
                for (i = 0; i < jumpMoves[j].length; i++) {
                    newX = pos(j, jumpMoves[j][i], x, y)[0];
                    newY = pos(j, jumpMoves[j][i], x, y)[1];
                    target = document.querySelector(`#g${newX}-${newY}`);
                    if (target) {
                        if (target.children.length != 0) {
                            other = findPiece(target.children[0]);
                            if (moverPiece.side != other.side) {
                                addMoveSquare(newX, newY);
                            }
                        } else {
                            addMoveSquare(newX, newY);
                        }
                    }
                    if (afterjumpMoves != []) {
                        let blocked = false;
                        let jumpX = newX;
                        let jumpY = newY;
                        let check = document.querySelector(`#g${jumpX}-${jumpY}`);
                        if (check && check.children.length == 0) {
                            for (r = 1; r <= afterjumpMoves[j]; r++) {
                                newX = pos(j, r, jumpX, jumpY)[0];
                                newY = pos(j, r, jumpX, jumpY)[1];
                                target = document.querySelector(`#g${newX}-${newY}`);
                                if (target && !blocked) {
                                    if (target.children.length != 0) {
                                        other = findPiece(target.children[0]);
                                        if (moverPiece.side != other.side) {
                                            addMoveSquare(newX, newY);
                                        }
                                        blocked = true;
                                    } else {
                                        addMoveSquare(newX, newY);
                                    }
                                } else {
                                    blocked = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //for range capture pieces
    if (rangeCapture != []) {
        for (c = 0; c < rangeCapture.length; c++) {
            let blocked = false;
            if (rangeCapture[c] != 0) {
                for (r = 1; r <= rangeCapture[c]; r++) {
                    newX = pos(c, r, x, y)[0];
                    newY = pos(c, r, x, y)[1];
                    target = document.querySelector(`#g${newX}-${newY}`);
                    if (target && !blocked) {
                        if (target.children.length != 0) {
                            other = findPiece(target.children[0]);
                            let moverRank = checkRank(mover.unit);
                            let otherRank = checkRank(other.unit);
                            if (moverRank >= otherRank) {
                                blocked = true;
                            } else {
                                addMoveSquare(newX, newY);
                            }
                        } else {
                            addMoveSquare(newX, newY);
                        }
                    } else {
                        blocked = true;
                    }
                }
            }
        }
    }
}

//move piece from one square to another
function moveSprite(move) {
    let unit = move[0];
    let destX = move[1];
    let destY = move[2];
    let oldNode = document.querySelector(`#g${unit.coords[1]}-${unit.coords[0]}`);
    let newNode = document.querySelector(`#g${destX}-${destY}`);
    let piece = document.querySelector(`#${unit.id}`);
    //if ranged capture, kill all
    if (checkRank(unit.unit) < 5) {
        let dir = 0;
        let diffX = unit.coords[1] - destX;
        let diffY = unit.coords[0] - destY;
        let steps = Math.abs(diffX);
        let modX = 0;
        let modY = 0;
        switch (true) {
            case (diffX == 0 && diffY > 0):
                dir = 1;
                steps = Math.abs(diffY);
                modY = -1;
                break;
            case (diffX < 0 && diffY > 0):
                dir = 2;
                modX = 1;
                modY = -1;
                break;
            case (diffX < 0 && diffY == 0):
                dir = 3;
                modX = 1;
                break;
            case (diffX < 0 && diffY < 0):
                dir = 4;
                modX = 1;
                modY = 1;
                break;
            case (diffX == 0 && diffY < 0):
                dir = 5;
                steps = Math.abs(diffY);
                modY = 1;
                break;
            case (diffX > 0 && diffY < 0):
                dir = 6;
                modX = -1;
                modY = 1;
                break;
            case (diffX > 0 && diffY == 0):
                dir = 7;
                modX = -1
                break;
            case (diffX > 0 && diffY > 0):
                dir = 8;
                modX = -1;
                modY = -1;
                break;
        }
        if (rangeCapture[dir - 1] != 0) {
            for (i = 1; i < steps; i++) {
                let space = document.querySelector(`#g${unit.coords[1] + (i * modX)}-${unit.coords[0] + (i * modY)}`);
                if (space.children.length != 0) {
                    let splicer = pieces.findIndex(obj => {
                        return obj.id == space.children[0].id;
                    });
                    space.removeChild(space.children[0]);
                    pieces.splice(splicer,1);
                }
            }
        }
    }
    //if space is occupied, capture
    if (newNode.children.length != 0) {
        let splicer = pieces.findIndex(obj => {
            return obj.id == newNode.children[0].id;
        });
        newNode.removeChild(newNode.children[0]);
        pieces.splice(splicer,1);
    }
    //move the piece
    newNode.appendChild(oldNode.removeChild(piece));
    //clear moving squares
    resetMoving();
    //update coords
    unit.coords[1] = destX;
    unit.coords[0] = destY;
    //check for promotion
    if ((unit.side == "sen" && unit.coords[0] < 12) || (unit.side == "go" && unit.coords[0] > 24)) {
        if (!newNode.children[0].children[0].classList.contains('promoted')) {
            promote(unit);
        }
    }
}

//the constellation of factored functions, arranged alphabetically
function addMoveSquare(x, y) {
    let blsq = document.querySelector(`#g${x}-${y}`);
    blsq.classList.add('moving');
    blsq.onclick = moveSprite.bind(this, [mover, x, y]);
}

function checkRank(unit) {
    switch (unit) {
        case "K": case "CP": return 1;
        case "GG": return 2;
        case "VG": return 3;
        case "RO": case "BG": case "VD": case "FLC": return 4;
        default: return 5;
    }
}

function findPiece(piece) {
    return pieces.find(obj => {
        return obj.id == piece.id;
    });
}

function pos(s, r, x, y) {
    switch (s) {
        case 0: return [x, y - r];
        case 1: return [x + r, y - r];
        case 2: return [x + r, y];
        case 3: return [x + r, y + r];
        case 4: return [x, y + r];
        case 5: return [x - r, y + r];
        case 6: return [x - r, y];
        case 7: return [x - r, y - r];
        
    }
}

function promote(piece) {
    //get raw piece data
    let data = pieceData.find(obj => {return obj.code == piece.unit;});
    let space = document.querySelector(`#g${piece.coords[1]}-${piece.coords[0]}`);
    space.children[0].children[0].classList.add('promoted');
    if (data.promote != "") {
        piece.unit = data.promote;
        let newUnit = pieceData.find(obj => {return obj.code == data.promote;});
        space.children[0].children[1].innerHTML = newUnit.name;
    }
}

function resetMoving() {
    if (document.querySelector('.highlight')) {
        const oldPiece = document.querySelector('.highlight');
        oldPiece.classList.remove('highlight');
        const oldMovers = document.querySelectorAll('.moving');
        for (i = 0; i < oldMovers.length; i++) {
            oldMovers[i].classList.remove('moving');
            oldMovers[i].onclick = null;
        }
    }
    stepMoves = [];
    jumpMoves = [];
    afterjumpMoves = [];
    rangeCapture = [];
}

//the grotesque logical horror beyond the edge of the stars
function getMoveLogic() {
    switch(mover.unit) {
        case "P":
            stepMoves = [1,0,0,0,0,0,0,0];
            break;
        case "EA": case "GB":
            stepMoves = [1,0,0,0,1,0,0,0];
            break;
        case "SG":
            stepMoves = [0,1,0,0,0,0,0,1];
            break;
        case "I": case "D":
            stepMoves = [1,1,0,0,0,0,0,1];
            break;
        case "OW": case "OR": case "ST":
            stepMoves = [1,0,0,1,0,1,0,0];
            break;
        case "T": case "SE":
            stepMoves = [0,1,0,0,1,0,0,1];
            break;
        case "C": case "FY": case "CM":
            stepMoves = [1,1,0,0,1,0,0,1];
            break;
        case "RD":
            stepMoves = [1,0,1,0,1,0,1,0];
            break;
        case "SN":
            stepMoves = [1,0,0,1,1,1,0,0];
            break;
        case "CK":
            stepMoves = [0,1,1,0,0,0,1,1];
            break;
        case "CS":
            stepMoves = [0,1,0,1,0,1,0,1];
            break;
        case "EW":
            stepMoves = [1,1,1,0,0,0,1,1];
            break;
        case "S": case "VS":
            stepMoves = [1,1,0,1,0,1,0,1];
            break;
        case "BI": case "CC":
            stepMoves = [0,1,1,0,1,0,1,1];
            break;
        case "OM":
            stepMoves = [0,1,0,1,1,1,0,1];
            break;
        case "G": case "NT":
            stepMoves = [1,1,1,0,1,0,1,1];
            break;
        case "FL":
            stepMoves = [1,1,0,1,1,1,0,1];
            break;
        case "BM": case "BB":
            stepMoves = [0,1,1,1,0,1,1,1];
            break;
        case "DE": case "NK": case "RSB":
            stepMoves = [1,1,1,1,0,1,1,1];
            break;
        case "DV":
            stepMoves = [1,0,1,1,1,1,1,1];
            break;
        case "DS":
            stepMoves = [1,1,1,1,1,1,1,0];
            break;
        case "BT":
            stepMoves = [0,1,1,1,1,1,1,1];
            break;
        case "P": case "LG": case "RG": case "BEE": case "VEN":
            stepMoves = [1,1,1,1,1,1,1,1];
            break;
        case "GN":
            stepMoves = [0,2,0,0,0,0,0,2];
            break;
        case "DO": case "EB":
            stepMoves = [2,0,2,0,2,0,2,0];
            break;
        case "FH":
            stepMoves = [0,2,0,2,0,2,0,2];
            break;
        case "BC":
            stepMoves = [2,2,2,2,0,2,2,2];
            break;
        case "K": case "FG": case "WE":
            stepMoves = [2,2,2,2,2,2,2,2];
            break;
        case "RB":
            stepMoves = [2,1,1,1,0,1,1,1];
            break;
        case "AB":
            stepMoves = [1,2,1,0,0,0,1,2];
            break;
        case "VB":
            stepMoves = [1,2,1,0,0,0,1,2];
            /* alternative moveset
            stepMoves = [0,2,1,1,0,1,1,2]; */
            break;
        case "ES": case "WS":
            stepMoves = [2,1,1,0,2,0,1,1];
            break;
        case "NB": case "SU": case "PR":
            stepMoves = [1,1,2,0,1,0,2,1];
            break;
        case "PS":
            stepMoves = [2,1,2,0,1,0,2,1];
            break;
        case "OK":
            stepMoves = [0,2,1,2,0,2,1,2];
            break;
        case "EG":
            stepMoves = [1,2,1,2,0,2,1,2];
            break;
        case "GU":
            stepMoves = [3,0,3,0,3,0,3,0];
            break;
        case "WR":
            stepMoves = [0,3,0,3,0,3,0,3];
            break;
        case "CT":
            stepMoves = [3,3,3,3,0,3,3,3];
            break;
        case "H": case "O": case "WN": case "RE":
            stepMoves = [3,1,0,0,1,0,0,1];
            break;
        case "F":
            stepMoves = [3,1,0,0,3,0,0,1];
            break;
        case "WG": case "M":
            stepMoves = [1,3,0,0,1,0,0,3];
            break;
        case "BD":
            stepMoves = [0,3,1,0,1,0,1,3];
            break;
        case "YA":
            stepMoves = [0,1,3,0,1,0,3,1];
            break;
        case "SWG":
            stepMoves = [3,3,0,0,1,0,0,3];
            break;
        case "CO":
            stepMoves = [2,3,2,3,0,3,2,3];
            break;
        case "BO":
            stepMoves = [3,3,2,3,0,3,2,3];
            break;
        case "HT":
            stepMoves = [4,4,4,4,4,4,4,4];
            break;
        case "CG": case "PG":
            stepMoves = [4,0,0,1,0,1,0,0];
            break;
        case "PI":
            stepMoves = [0,4,0,0,2,0,0,4];
            break;
        case "MS":
            stepMoves = [1,3,2,0,4,0,2,3];
            break;
        case "LPK":
            stepMoves = [5,5,5,5,5,5,5,5];
            break;
        case "TD":
            stepMoves = [0,5,1,0,1,0,1,5];
            break;
        case "SC":
            stepMoves = [5,3,3,0,1,0,3,3];
            break;
        case "BN":
            stepMoves = [7,5,3,0,1,0,3,5];
            break;
        case "L": case "OC": case "TG":
            stepMoves = [40,0,0,0,0,0,0,0];
            break;
        case "RV":
            stepMoves = [40,0,0,0,40,0,0,0];
            break;
        case "SI":
            stepMoves = [40,0,40,0,0,0,40,0];
            break;
        case "MNW":
            stepMoves = [0,0,0,40,40,40,0,0];
            break;
        case "WH": case "BOP": case "MG":
            stepMoves = [40,40,0,0,40,0,0,40];
            break;
        case "R": case "SO": case "RH": case "SQ": case "GLS":
            stepMoves = [40,0,40,0,40,0,40,0];
            break;
        case "FRS": case "COD": case "W":
            stepMoves = [40,0,0,40,40,40,0,0];
            break;
        case "B":
            stepMoves = [0,40,0,40,0,40,0,40];
            break;
        case "FRW": case "RUL":
            stepMoves = [40,40,40,0,0,0,40,40];
            break;
        case "WIZ":
            stepMoves = [0,40,40,0,40,0,40,40];
            break;
        case "OX": case "FRB": case "FRL": case "GW":
            stepMoves = [40,40,0,40,40,40,0,40];
            break;
        case "TF":
            stepMoves = [40,40,0,40,40,40,0,40];
            /* alternative moveset
            stepMoves = [40,40,0,40,40,40,0,40];
            jumpMoves = [[2,3],[2,3],0,[2,3],[2,3],[2,3],0,[2,3]]; */
            break;
        case "CV": case "STC":
            stepMoves = [40,40,40,0,40,0,40,40];
            break;
        case "FRG": case "FRT":
            stepMoves = [0,40,40,40,40,40,40,40];
            break;
        case "Q": case "FRA": case "STE":
            stepMoves = [40,40,40,40,40,40,40,40];
            break;
        case "HDL": case "HDR":
            stepMoves = [40,0,0,0,1,0,0,0];
            break;
        case "VH":
            stepMoves = [40,1,0,0,1,0,0,1];
            break;
        case "SP":
            stepMoves = [40,0,1,0,1,0,1,0];
            break;
        case "VP":
            stepMoves = [40,0,0,1,1,1,0,0];
            break;
        case "RAF":
            stepMoves = [40,1,1,0,0,0,1,1];
            break;
        case "RIC":
            stepMoves = [1,0,1,0,1,40,1,0];
            break;
        case "LIC":
            stepMoves = [1,0,1,40,1,0,1,0];
            break;
        case "VL":
            stepMoves = [40,1,1,0,1,0,1,1];
            break;
        case "RDO":
            stepMoves = [40,0,0,0,1,40,0,0];
            break;
        case "LDO":
            stepMoves = [40,0,0,40,1,0,0,0];
            break;
        case "HE": case "FS":
            stepMoves = [0,40,0,0,1,0,0,40];
            break;
        case "WC":
            stepMoves = [40,0,0,1,40,0,0,1];
            break;
        case "TC":
            stepMoves = [40,1,0,0,40,1,0,0];
            break;
        case "RUB": case "RP": case "RU": case "EC": case "VM":
            stepMoves = [40,0,1,0,40,0,1,0];
            break;
        case "VO":
            stepMoves = [1,40,0,0,1,0,0,40];
            break;
        case "WF":
            stepMoves = [0,0,40,1,0,0,40,1];
            break;
        case "SX":
            stepMoves = [0,1,40,0,0,1,40,0];
            break;
        case "SM":
            stepMoves = [1,0,40,0,1,0,40,0];
            break;
        case "MK":
            stepMoves = [0,1,40,0,1,0,40,1];
            break;
        case "DIS":
            stepMoves = [1,0,1,0,1,40,1,40];
            /* alternative moveset
            stepMoves = [1,0,1,40,1,40,1,40]; */
            break;
        case "PLO":
            stepMoves = [40,1,0,1,40,1,0,1];
            break;
        case "SW":
            stepMoves = [1,1,40,0,1,0,40,1];
            break;
        case "SF":
            stepMoves = [0,1,40,1,0,1,40,1];
            break;
        case "FLS": case "COP":
            stepMoves = [40,1,1,1,40,1,1,1];
            break;
        case "VI":
            stepMoves = [1,1,1,40,1,1,1,40];
            break;
        case "TS":
            stepMoves = [1,40,1,1,1,40,1,1];
            break;
        case "SA":
            stepMoves = [1,1,40,1,1,1,40,1];
            break;
        case "LC":
            stepMoves = [40,0,0,40,0,0,1,40];
            break;
        case "RC":
            stepMoves = [40,40,1,0,0,40,0,0];
            break;
        case "GRT":
            stepMoves = [1,0,40,0,40,0,40,0];
            break;
        case "TT":
            stepMoves = [0,1,0,1,0,40,40,40];
            break;
        case "LT":
            stepMoves = [0,40,40,40,0,1,0,1];
            break;
        case "GRB":
            stepMoves = [40,40,1,0,1,0,1,40];
            break;
        case "RR":
            stepMoves = [40,40,0,1,1,1,0,40];
            break;
        case "LAR":
            stepMoves = [1,1,1,1,1,40,40,40];
            break;
        case "RAR":
            stepMoves = [1,40,40,40,1,1,1,1];
            break;
        case "DIU":
            stepMoves = [1,40,1,40,1,40,1,1];
            break;
        case "RW":
            stepMoves = [1,40,40,0,0,0,40,40];
            break;
        case "FLF":
            stepMoves = [1,40,0,40,0,40,0,40];
            break;
        case "BNC":
            stepMoves = [40,40,1,0,40,0,1,40];
            break;
        case "DK":
            stepMoves = [40,1,40,1,40,1,40,1];
            break;
        case "DH":
            stepMoves = [1,40,1,40,1,40,1,40];
            break;
        case "FRO":
            stepMoves = [40,40,40,0,1,0,40,40];
            break;
        case "WD":
            stepMoves = [0,40,40,40,0,1,40,40];
            break;
        case "CL":
            stepMoves = [1,40,1,40,40,40,1,40];
            break;
        case "RA":
            stepMoves = [1,1,40,40,40,40,40,1];
            break;
        case "FIO": case "VNW":
            stepMoves = [40,40,1,40,40,40,1,40];
            break;
        case "CNR":
            stepMoves = [1,40,40,40,1,40,40,40];
            break;
        case "VT":
            stepMoves = [40,0,0,0,2,0,0,0];
            break;
        case "WST":
            stepMoves = [40,2,0,0,40,0,0,2];
            break;
        case "RUT": case "RT": case "BA":
            stepMoves = [40,0,2,0,40,0,2,0];
            break;
        case "GL":
            stepMoves = [0,40,0,2,0,2,0,40];
            break;
        case "SR":
            stepMoves = [0,2,0,40,0,40,0,2];
            break;
        case "WAH":
            stepMoves = [40,2,2,0,40,0,2,2];
            break;
        case "YOU":
            stepMoves = [40,0,2,2,40,2,2,0];
            break;
        case "RI":
            stepMoves = [0,0,2,0,0,40,40,40];
            break;
        case "LE":
            stepMoves = [0,40,40,40,0,0,2,0];
            break;
        case "BL":
            stepMoves = [40,40,2,0,40,0,2,0];
            break;
        case "WT":
            stepMoves = [2,0,40,0,2,0,40,40];
            break;
        case "DIT":
            stepMoves = [40,0,40,0,2,0,40,40];
            break;
        case "DID":
            stepMoves = [40,40,40,0,40,0,2,0];
            break;
        case "RN":
            stepMoves = [0,40,40,0,2,0,40,40];
            break;
        case "RS":
            stepMoves = [40,2,40,2,40,2,40,2];
            break;
        case "CD": case "EK":
            stepMoves = [2,40,2,40,2,40,2,40];
            break;
        case "WL": case "FRC": case "HO": case "GRH":
            stepMoves = [40,40,2,0,40,0,2,40];
            break;
        case "FRD":
            stepMoves = [40,40,2,2,40,2,2,40];
            break;
        case "RUO":
            stepMoves = [40,40,40,2,0,2,40,40];
            break;
        case "CH":
            stepMoves = [40,40,2,40,40,40,2,40];
            break;
        case "DM": case "WB":
            stepMoves = [2,40,40,40,2,40,40,40];
            break;
        case "STB":
            stepMoves = [40,40,40,40,2,40,40,40];
            break;
        case "LH":
            stepMoves = [40,1,0,0,2,0,0,1];
            break;
        case "VE": case "VR":
            stepMoves = [40,0,2,0,1,0,2,0];
            break;
        case "TIS":
            stepMoves = [2,40,0,0,1,0,0,40];
            break;
        case "ED":
            stepMoves = [1,2,0,40,0,40,0,2];
            /* alternative moveset
            stepMoves = [2,1,0,40,1,40,0,1]; */
            break;
        case "SV":
            stepMoves = [40,2,0,1,40,1,0,2];
            break;
        case "CI":
            stepMoves = [40,1,2,0,40,0,2,1];
            break;
        case "SL":
            stepMoves = [2,0,40,0,1,0,40,0];
            break;
        case "GC":
            stepMoves = [40,1,2,1,40,1,2,1];
            break;
        case "BS": case "LP": case "BE":
            stepMoves = [40,40,2,0,1,0,2,40];
            break;
        case "FP": case "FRX": case "FRH": case "FRP":
            stepMoves = [40,40,2,1,40,1,2,40];
            break;
        case "LS":
            stepMoves = [40,2,40,1,40,1,40,2];
            break;
        case "CR":
            stepMoves = [40,3,0,0,40,0,0,3];
            break;
        case "FO":
            stepMoves = [3,40,3,0,40,0,3,40];
            break;
        case "GD":
            stepMoves = [3,40,0,40,3,40,0,40];
            break;
        case "CN": case "SD":
            stepMoves = [40,3,40,3,40,3,40,3];
            break;
        case "GR":
            stepMoves = [3,40,3,40,3,40,3,40];
            break;
        case "GE":
            stepMoves = [40,40,40,3,40,3,40,40];
            break;
        case "VW":
            stepMoves = [40,0,1,0,3,0,1,0];
            break;
        case "SS":
            stepMoves = [3,0,40,0,1,0,40,0];
            break;
        case "CE":
            stepMoves = [40,3,1,0,40,0,1,3];
            break;
        case "GOW":
            stepMoves = [40,1,3,1,40,1,3,1];
            break;
        case "HS": case "OS":
            stepMoves = [40,40,3,0,1,0,3,40];
            break;
        case "SPG":
            stepMoves = [40,0,3,0,2,0,3,0];
            break;
        case "BNG":
            stepMoves = [40,40,3,0,2,0,3,40];
            break;
        case "BBR": case "CB":
            stepMoves = [40,40,3,40,2,40,3,40];
            break;
        case "LB": case "GRL":
            stepMoves = [40,3,2,0,1,0,2,3];
            break;
        case "TR":
            stepMoves = [40,40,4,0,4,0,4,40];
            break;
        case "FI":
            stepMoves = [40,4,40,2,40,2,40,4];
            break;
        case "WA":
            stepMoves = [40,2,40,4,40,4,40,2];
            break;
        case "LBG":
            stepMoves = [40,40,5,0,40,0,5,40];
            break;
        case "RPH":
            stepMoves = [0,40,5,40,0,40,5,40];
            break;
        case "PCM":
            stepMoves = [5,40,5,40,0,40,5,40];
            break;
        case "FR":
            stepMoves = [0,40,40,40,5,40,0,40];
            /* alternative moveset
            stepMoves = [5,40,40,40,5,40,40,40]; */
            break;
        case "FT":
            stepMoves = [40,40,5,40,40,40,5,40];
            break;
        case "FF":
            stepMoves = [5,40,40,40,5,40,40,40];
            break;
        case "RUD":
            stepMoves = [40,40,40,40,5,40,40,40];
            break;
        case "GRS":
            stepMoves = [40,5,40,2,40,2,40,5];
            break;
        case "CBG":
            stepMoves = [40,5,3,0,2,0,3,5];
            break;
        case "PK":
            stepMoves = [40,3,5,2,40,2,5,3];
            break;
        case "N":
            possibleMoves.push([mover.coords[1] - 1, mover.coords[0] - 2]);
            possibleMoves.push([mover.coords[1] + 1, mover.coords[0] - 2]);
            break;
        case "FD":
            jumpMoves = [0,[2],0,[2],0,[2],0,[2]];
            break;
        case "KR":
            stepMoves = [0,1,0,1,0,1,0,1];
            jumpMoves = [0,0,[2],0,0,0,[2],0];
            break;
        case "PH":
            stepMoves = [1,0,1,0,1,0,1,0];
            jumpMoves = [0,[2],0,[2],0,[2],0,[2]];
            break;
        case "FC":
            stepMoves = [0,0,0,1,1,1,0,0];
            jumpMoves = [[3],[3],[3],0,0,0,[3],[3]];
            break;
        case "HR":
            stepMoves = [40,40,0,0,1,0,0,40];
            jumpMoves = [0,0,0,[2],0,[2],0,0];
            break;
        case "MF":
            stepMoves = [40,40,40,2,40,2,40,40];
            jumpMoves = [[2],0,0,0,0,0,0,0];
            /* alternative moveset
            stepMoves = [40,40,0,2,40,2,0,40];
            jumpMoves = [[2],0,0,0,0,0,0,0]; */
            break;
        case "LL":
            stepMoves = [40,40,2,40,40,40,2,40];
            jumpMoves = [[2],0,0,0,[2],0,0,0];
            break;
        case "GS":
            stepMoves = [40,0,40,2,40,2,40,0];
            jumpMoves = [0,[2],0,0,0,0,0,[2]];
            break;
        case "ML":
            stepMoves = [40,40,40,2,40,40,40,40];
            jumpMoves = [0,0,0,0,0,[2],0,[2]];
            break;
        case "MR":
            stepMoves = [40,40,40,40,40,2,40,40];
            jumpMoves = [0,[2],0,[2],0,0,0,0];
            break;
        case "KM": case "GT":
            stepMoves = [40,40,3,40,40,40,3,40];
            jumpMoves = [[3],0,0,0,[3],0,0,0];
            break;
        case "PM":
            stepMoves = [40,40,3,40,40,40,3,40];
            jumpMoves = [0,[3],0,0,0,0,0,[3]];
            break;
        case "GM":
            stepMoves = [40,40,5,5,40,5,5,40];
            jumpMoves = [[3],[3],0,0,0,0,0,[3]];
            break;
        case "HF":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[2],0,0,0,0,0,0,0];
            break;
        case "EL":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [0,[2],0,0,0,0,0,[2]];
            break;
        case "DG":
            stepMoves = [40,40,40,3,40,3,40,40];
            jumpMoves = [[3],0,[3],0,[3],0,[3],0];
            break;
        case "LD":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[3],[3],[3],[3],[3],[3],[3],[3]];
            break;
        case "GDE":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [0,0,[3],0,0,0,[3],0];
            break;
        case "HH":
            stepMoves = [40,0,0,0,0,0,0,0];
            possibleMoves.push([mover.coords[1] - 1, mover.coords[0] - 2]);
            possibleMoves.push([mover.coords[1] + 1, mover.coords[0] - 2]);
            possibleMoves.push([mover.coords[1] - 1, mover.coords[0] + 2]);
            possibleMoves.push([mover.coords[1] + 1, mover.coords[0] + 2]);
            break;
        case "SPT":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[3],0,[3],0,[3],0,[3],0];
            break;
        case "TRT":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[2],0,[2],0,[2],0,[2],0];
            break;
        case "WO":
            stepMoves = [2,40,2,40,2,40,2,40];
            jumpMoves = [0,[3],0,[3],0,[3],0,[3]];
            afterjumpMoves = [0,2,0,2,0,2,0,2];
            break;
        case "MT":
            stepMoves = [40,40,3,3,40,3,3,40];
            jumpMoves = [[2],[2],0,0,[2],0,0,[2]];
            afterjumpMoves = [40,40,0,0,40,0,0,40];
            /* alternative moveset
            stepMoves = [0,0,3,3,0,3,3,0];
            jumpMoves = [[2],[2],0,0,[2],0,0,[2]];
            afterjumpMoves = [40,40,0,0,40,0,0,40]; */
            break;
        case "RM":
            stepMoves = [40,0,5,5,40,5,5,0];
            jumpMoves = [0,[3],0,0,0,0,0,[3]];
            afterjumpMoves = [0,40,0,0,0,0,0,40];
            break;
        case "FE":
            stepMoves = [0,0,0,0,0,0,0,0];
            jumpMoves = [[2,3],[2,3,4],[2,3],[2,3],[2,3],[2,3],[2,3],[2,3,4]];
            afterjumpMoves = [40,40,40,40,40,40,40,40];
            break;
        case "FRI":
            stepMoves = [40,0,40,3,40,3,40,0];
            jumpMoves = [0,[1,2,3],0,0,0,0,0,[1,2,3]];
            afterjumpMoves = [0,40,0,0,0,0,0,40];
            break;
        case "GRF":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[2],0,0,0,[2],0,0,0];
            afterjumpMoves = [40,0,0,0,40,0,0,0];
            break;
        case "TK":
            jumpMoves = [[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3]];
            afterjumpMoves = [40,40,40,40,40,40,40,40];
            break;
        case "MNC":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[3],[3],[3],[3],[3],[3],[3],[3]];
            afterjumpMoves = [40,40,40,40,40,40,40,40];
            break;
        case "GRE":
            stepMoves = [40,40,40,40,40,40,40,40];
            jumpMoves = [[2],[2],[2],[2],[2],[2],[2],[2]];
            afterjumpMoves = [40,40,40,40,40,40,40,40];
            break;
        case "GEL":
            stepMoves = [40,3,40,40,40,40,40,3];
            jumpMoves = [[1,2,3],0,[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],0];
            afterjumpMoves = [40,0,40,40,40,40,40,0];
            break;
        case "GO":
            stepMoves = [40,0,3,3,40,3,3,0];
            jumpMoves = [0,[1,2,3],0,0,0,0,0,[1,2,3]];
            afterjumpMoves = [0,40,0,0,0,0,0,40];
            break;
        case "AND":
            stepMoves = [0,40,0,40,0,40,0,40];
            jumpMoves = [
                [
                    1,2,3,4,5,6,7,8,9,10,
                    11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30,
                    31,32,33,34,35,36,37,38,39,40
                ],0,0,0,[
                    1,2,3,4,5,6,7,8,9,10,
                    11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30,
                    31,32,33,34,35,36,37,38,39,40
                ],0,0,0
            ];
            break;
        case "RAD":
            stepMoves = [3,0,2,0,40,0,2,0];
            jumpMoves = [
                0,[
                    1,2,3,4,5,6,7,8,9,10,
                    11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30,
                    31,32,33,34,35,36,37,38,39,40
                ],0,0,0,0,0,[
                    1,2,3,4,5,6,7,8,9,10,
                    11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30,
                    31,32,33,34,35,36,37,38,39,40
                ]
            ];
            break;
        case "RO":
            rangeCapture = [40,0,40,0,40,0,40,0];
            break;
        case "BG":
            rangeCapture = [0,40,0,40,0,40,0,40];
            break;
        case "GG":
            rangeCapture = [40,40,40,40,40,40,40,40];
            break;
        case "VD":
            stepMoves = [2,0,2,0,2,0,2,0];
            rangeCapture = [0,40,0,40,0,40,0,40];
            break;
        case "FLC":
            stepMoves = [0,3,0,2,0,2,0,3];
            rangeCapture = [40,0,40,0,40,0,40,0];
            break;
        case "VG":
            jumpMoves = [[2],0,[2],0,[2],0,[2],0];
            rangeCapture = [0,40,0,40,0,40,0,40];
            break;
        case "HM":
            return;
        case "LO":
            return;
        case "CA":
            return;
        case "PC":
            return;
        case "HTK":
            return;
        case "LN":
            return;
        case "FUF":
            return;
        case "BUD":
            return;
        case "LI":
            return;
        default:
            stepMoves = [1,1,1,1,1,1,1,1];
    }
}